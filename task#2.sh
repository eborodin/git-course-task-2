#!/usr.bin/bash

for branch in $(git branch -r --merged master | grep origin | grep -v develop | grep -v master)
do
  git push origin --delete "${branch#*/}"
done